import nextcord
import random
import aiohttp
from nextcord.ext import commands


class Fun(commands.Cog):

    def __init__(self, client):
        self.client = client

    @commands.command(aliases=["hello", "greet"])
    async def hi(self, ctx):
        await ctx.message.delete()
        await ctx.send("Say hello!", delete_after=15)

        def check(m):
            val = False
            if "hello" in str(m.content).lower() and m.channel == ctx.channel:
                val = True
            elif "hi" in str(m.content).lower() and m.channel == ctx.channel:
                val = True
            return(val)

        msg = await self.client.wait_for("message", check=check, timeout=15)
        await ctx.send(f"Hello {msg.author}!")

    @commands.command(aliases=['roll'])
    async def dice(self, ctx):
        await ctx.message.delete()
        await ctx.send(f'And the dice lands on.... {random.randint(1,6)}!')

    @commands.command(pass_content=True)
    async def id(self, ctx, user: nextcord.Member):
        await ctx.send(f"{user}'s id is {user.id}")
        await ctx.message.delete()

    @commands.command(aliases=["msg", "message"])
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def send(self, ctx, user: nextcord.Member, *, msg):
        if user == ctx.message.author:
            await ctx.send("You... want to send a message to yourself?")
        else:
            try:
                emb = nextcord.Embed(
                    colour=nextcord.Colour.blue()
                )
                emb.add_field(name=f"Message recived from {ctx.message.guild.name}", value=f"{msg}")
                emb.set_footer(text=f"Message sent by {ctx.message.author}")
                await user.send(embed=emb)

                embed = nextcord.Embed(
                    colour=nextcord.Colour.green()
                )
                embed.add_field(name=f"Message sent to {user} from {ctx.message.author}", value=f"{msg}")
                await ctx.channel.send(embed=embed)
                await ctx.message.delete()
            except:
                emer = nextcord.Embed(
                    color=nextcord.Colour.red()
                )
                emer.set_author(name="Aww Snap! Something went wrong..")
                emer.set_footer(
                    text="This may be because the recepient has closed DMs or the message was too long")
                await ctx.send(embed=emer)

    @commands.command()
    async def game(self, ctx):
        await ctx.send("This command is currently unavalible.")

    @commands.command()
    async def meme(ctx):
        async with aiohttp.ClientSession() as cs:
            async with cs.get('https://www.reddit.com/r/memes.json') as r:
                memes = await r.json()
                embed = nextcord.Embed(color=nextcord.Color.blue())
                embed.set_image(url=memes['data']['children'][random.randint(
                    0, 25)]['data']['url'])
                embed.set_footer()
                await ctx.send(embed=embed)


def setup(client):
    client.add_cog(Fun(client))
