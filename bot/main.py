import nextcord
from nextcord.ext import commands, tasks
import os
# import pymongo
# from pymongo import MongoClient

# mongodb, PIP INSTALL PYMONGO, AND PIP INSTALL Python-DOTENV
# Make sure to have a mongodb server there free until 500mb

TOKEN = os.environ['TOKEN']

#MONGODB_URI = os.environ['MONGOURI']
#COLLECTION = os.getenv("COLLECTION")
#DB_NAME = os.getenv("DBNAME")

client = commands.Bot(command_prefix="h!")


# Change For Where Bot Is Actually Hosted
# For Economy

intents = nextcord.Intents.all()
client = commands.Bot(command_prefix='h!', intents=intents)


@client.event
async def on_ready():
    print('Bot is Online')
    await client.change_presence(activity=nextcord.Game(f'h!help in {len(client.guilds)} guilds'))

for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')

client.run(TOKEN)
